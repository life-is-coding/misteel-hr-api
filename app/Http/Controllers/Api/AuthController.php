<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Models\Menu;
use \Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{

    public function login(Request $request)
    {
        $key = 'SECRET';

        $last_login = date("Y-m-d H:i:s");

        $input = request()->only(['username', 'password']);
        if (array_key_exists('username', $input) && array_key_exists('password', $input) && auth()->validate($input)) {
            $payload = [
                'username' => $input['username'],
                'last_login' => $last_login
            ];
            $token = JWT::encode($payload, $key, 'HS256');
            $data = auth()->user($token);

            $fullname = $data->first_name_en . " " . $data->last_name_en;
            $user_type = $data->user_type;
            $position_id = $data->position_id;
            $position_en = $data->position_name_en;
            $position_th = $data->position_name_th;
            $sys_lang = $data->sys_lang;

            // $file_store_name = $data->name;
            $username = $data->name;
            $password_updated_at = $data->updated_at;

            $status = "ok"; // ไม่เปลี่ยนพาส
            $message = "";
            $message_alert = "";

            if (Carbon::now()->diffInMonths($password_updated_at) >= 6) {
                $status = "expired"; // บังคับเปลี่ยนพาส 
                $message = "Your password was expired. Please change password before access to system.";
            } else {
                $days = Carbon::now()->diffInDays($password_updated_at);
                $max = 180;
                $diff = $max - $days;
                if ($diff == 30 || $diff == 15 || $diff == 3) {
                    $message_alert = "Your password will be expired in " . $diff . " days, Please reset password in profile setting.";
                }
            }

            // User::where('name', $username)->update([
            //     "remember_token" => $token,
            // "last_login" => $last_loginx
            // ]);

            $res = array();
            $res['status'] = $status;
            $res['message'] = $message;
            $res['message_alert'] = $message_alert;
            $res['sys_lang'] = $sys_lang;
            $res['fullname'] = $fullname;
            $res['user_type'] = $user_type;
            $res['position_id'] = $position_id;
            $res['position_en'] = $position_en;
            $res['position_th'] = $position_th;
            $res['token'] = $token;
        } else {
            $res = array();
            $res['status'] = 'error';
            $res['message'] = 'Username or Password wrong! Please try again';
        }
        return response()->json($res, 200);
    }

    // public function check_email(Request $request)
    // {
    //     $input = request()->all();
    //     $email = $input['email'];
    //     $date_today = date('Y-m-d H:i:s');

    //     $query_email =  Users::select('name', 'email')->where('email', '=', $email)->first();

    //     if (empty($query_email)) {
    //         return response([], 404);
    //     } else {

    //         $all_md5 = $email . $date_today;
    //         $md5 = md5($all_md5);

    //         DB::table('password_resets')
    //             ->insert([
    //                 'email' => $email, 'token' => $md5, 'created_at' => $date_today
    //             ]);

    //         $data = array(
    //             'tk' => $md5
    //         );

    //         // ส่ง email -- subject คือ หัวข้อหรือ title ในการส่ง
    //         Mail::send(
    //             'resetpassword.send_mail',
    //             $data,
    //             function ($message) use ($email) {
    //                 $message->to($email)->subject('Reset password Hybrid Pro.');
    //             }
    //         );

    //         return "email sent";
    //     }
    // }

    // public function check_mail_token($mail_token)
    // {
    //     $check = DB::table('password_resets')->where('token', $mail_token)->select('email')->first();

    //     if (!empty($check)) {
    //         $data =  Users::select('id', 'name AS username', 'email')->where('email', '=', $check->email)->first();
    //         return response()->json($data);
    //     } else {
    //         return response([], 404);
    //     }
    // }

    // public function update_password(Request $request, $id)
    // {
    //     $input = request()->all();

    //     try {
    //         Users::where('id', $id)
    //             ->update([
    //                 'password' => Hash::make($input['password']),
    //                 'updated_at' => date('Y-m-d H:i:s')
    //             ]);
    //         DB::table('password_resets')->where('email', $input['email'])->delete();

    //         return "update - complete";
    //     } catch (Exception $e) {
    //         return response($e->getMessage(), 500);
    //     }
    // }

    public function change_password(Request $request)
    {
        $token = request()->bearerToken();
        $decoded = auth()->user($token);
        $username = $decoded->name;
        $password = $request->password;

        $fullname = $decoded->first_name_en . " " . $decoded->last_name_en;
        $user_type = $decoded->user_type;
        $position_id = $decoded->position_id;
        $position_en = $decoded->position_name_en;
        $position_th = $decoded->position_name_th;
        $sys_lang = $decoded->sys_lang;

        try {
            User::where('name', $username)
                ->update([
                    'password' => Hash::make($password),
                    'updated_at' => date('Y-m-d H:i:s')
                ]);

            $status = "ok";
            $message = "Change password completed";
        } catch (Exception $e) {
            $status = "error";
            $message = $e->getMessage();
        }

        $res = array();
        $res['status'] = $status;
        $res['message'] = $message;
        $res['sys_lang'] = $sys_lang;
        $res['fullname'] = $fullname;
        $res['user_type'] = $user_type;
        $res['position_id'] = $position_id;
        $res['position_en'] = $position_en;
        $res['position_th'] = $position_th;

        return response()->json($res);
    }

    public function change_lang(Request $request)
    {
        $token = request()->bearerToken();
        $decoded = auth()->user($token);
        $username = $decoded->name;

        $sys_lang = $request->lang;

        try {
            User::where('name', $username)
                ->update([
                    'sys_lang' => $sys_lang
                ]);

            $status = "ok";
            $message = "Change language completed";
        } catch (Exception $e) {
            $status = "error";
            $message = $e->getMessage();
        }
        $res = array();
        $res['status'] = $status;
        $res['message'] = $message;
        return response()->json($res);
    }
    // public function check_auth(Request $request)
    // {

    //     $token = request()->bearerToken();
    //     $decoded = auth()->user($token);
    //     $username = $decoded->name;
    //     $user_id = $decoded->id;

    //     $input = $request->all();
    //     $url = $input['url'];
    //     $path = str_replace('http://localhost:8080', '', $url);
    //     $path = str_replace('https://aitt.netlify.app', '', $path);
    //     $path = str_replace('https://app.aittservice.com', '', $path);
    //     $permissions = SubmenuPermission::join('permission_type', 'permission_type.permission_type_id', 'submenu_permissions.permission_type_id')
    //         ->select('permission_type_name')
    //         ->where('user_id', $user_id)
    //         ->whereIn('submenu_id', [DB::raw(" select submenu_id from submenu where path = '{$path}' ")])
    //         ->first();

    //     $status = 'ok';
    //     if (!isset($permissions->permission_type_name) || $permissions->permission_type_name == 'disabled') {
    //         $status = 'error';
    //     }

    //     return response()->json(compact('status'));
    // }

    public function get_menu(Request $request)
    {
        $token = request()->bearerToken();
        $decoded = auth()->user($token);
        $user_type = $decoded->user_type;

        $data = Menu::with('subs')
            ->select(
                'menu_index',
                'menu_icon',
                'menu_name',
                DB::raw(" 'main' as type ")
            )
            ->whereRaw($user_type . " = 1 ")
            ->orderBy('sequence')
            ->distinct()
            ->get();

        return response()->json($data);
    }
}
