<?php

namespace App\Http\Controllers\Api\Form;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Form\FormRequest;

class RequestController extends Controller
{
    public function view(Request $request)
    {
        $token = request()->bearerToken();
        $decoded = auth()->user($token);
        $username = $decoded->name;

        $data = FormRequest::join('form_approval', 'form_approval.approval_id', 'form_request.approval_id')
            ->join('form_process', 'form_process.form_id', 'form_approval.form_id')
            ->join('form_status', 'form_status.status_id', 'form_approval.form_status')
            ->select(
                '*',
                DB::raw("( select concat(first_name_en,' ',last_name_en) from users where name = created_by ) as created_name"),
                DB::raw("( select department_name_en from departments where department_id in ( select department_id from users where name = created_by ) ) as department_name_en"),
                DB::raw("( DATE_FORMAT(created_at,'%e-%b-%y %H:%i') ) as created_date"),
            )
            ->where('created_by', $username)
            ->get();

        return response()->json($data);
    }

    public function save(Request $request)
    {
        $token = request()->bearerToken();
        $decoded = auth()->user($token);
        $username = $decoded->name;

        $date = date("Y-m-d H:i:s");

        $approval_id = DB::table('form_approval')->insertGetId([
            'created_at' => $date,
            'created_by' => $username,
            'form_id' => $request->type,
            'form_status' => 1
        ]);

        $new = new FormRequest();
        $new->approval_id = $approval_id;
        $new->type = $request->type;

        if ($request->type == 1) {
            $new->item = $request->item;
            $new->purpose = $request->purpose;
        }

        if ($request->type == 2) {
            $new->destination = $request->destination;
            $new->phone_no = $request->phone_no;
            $new->departure_date = $request->departure_date;
            $new->departure_time = $request->departure_time;
            $new->arrival_date = $request->arrival_date;
            $new->arrival_time = $request->arrival_time;
            $new->internet = $request->internet;
            $new->voice = $request->voice;
            $new->sms = $request->sms;
        }

        if ($request->type == 3) {
            $new->domain = $request->domain;
            $new->sbo = $request->sbo;
            $new->user_type = $request->user_type;
            $new->user_group = $request->user_group;
            $new->reason = $request->reason;
            $new->others_reason = $request->others_reason;
        }
        $new->save();

        return response()->json('ok');
    }
}
