<?php

namespace App\Http\Controllers\Api\Main;

use DB;
use Exception;
use App\Models\Users;
use App\Models\UserFamilies;
use Illuminate\Http\Request;
use App\Models\Setting\Agencies;
use App\Models\Setting\Positions;
use App\Models\Setting\Departments;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class EmployeeController extends Controller
{
    public function active(Request $request)
    {
        try {
            $data = array();
            $data['agencies'] = Agencies::where('agency_status', '1')
                ->orderBy('agency_name_en')
                ->get();
            $data['departments'] = Departments::where('department_status', '1')
                ->orderBy('department_name_en')
                ->get();
            $data['positions'] = Positions::where('position_status', '1')
                ->orderBy('position_name_en')
                ->get();
        } catch (Exception $e) {
            return response($e->getMessage(), 500);
        }

        return response()->json($data);
    }

    public function create(Request $request)
    {
        try {
            $user_id = DB::table('users')->insertGetId([
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
                'first_name_en' => $request->first_name_en,
                'status' => 1,
                'emp_id' => NULL,
                'start_date' => $request->start_date,
                'birth_date' => $request->birth_date,
                'gender' => $request->gender,
                'last_name_en' => $request->last_name_en,
                'first_name_th' => $request->first_name_th,
                'last_name_th' => $request->last_name_th,
                'user_type' => $request->user_type,
                'agency_id' => $request->agency_id,
                'department_id' => $request->department_id,
                'position_id' => $request->position_id,
            ]);

            foreach ($request->families as $family) {
                $new = new UserFamilies();
                $new->user_id = $user_id;
                $new->relationship = $family['relationship'];
                $new->child_no = $family['child_no'];
                $new->family_first_name_en = $family['family_first_name_en'];
                $new->family_last_name_en = $family['family_last_name_en'];
                $new->family_first_name_th = $family['family_first_name_th'];
                $new->family_last_name_th = $family['family_last_name_th'];
                $new->family_birth_date = $family['family_birth_date'];
                $new->save();
            }
        } catch (Exception $e) {
            return response($e->getMessage(), 500);
        }

        return response()->json('complete');
    }

    public function update(Request $request, $id)
    {
        try {
            Users::where('id', $id)->update([
                'name' => $request->name,
                'email' => $request->email,
                'first_name_en' => $request->first_name_en,
                'last_name_en' => $request->last_name_en,
                'first_name_th' => $request->first_name_th,
                'last_name_th' => $request->last_name_th,
                'user_type' => $request->user_type,
                'agency_id' => $request->agency_id,
                'start_date' => $request->start_date,
                'birth_date' => $request->birth_date,
                'gender' => $request->gender,
                'department_id' => $request->department_id,
                'position_id' => $request->position_id,
            ]);

            foreach ($request->families as $family) {
                if (!isset($family['family_id'])) {
                    $new = new UserFamilies();
                    $new->user_id = $id;
                    $new->relationship = $family['relationship'];
                    $new->child_no = $family['child_no'];
                    $new->family_first_name_en = $family['family_first_name_en'];
                    $new->family_last_name_en = $family['family_last_name_en'];
                    $new->family_first_name_th = $family['family_first_name_th'];
                    $new->family_last_name_th = $family['family_last_name_th'];
                    $new->family_birth_date = $family['family_birth_date'];
                    $new->save();
                } else {
                    UserFamilies::where('family_id', $family['family_id'])->update([
                        'relationship' => $family['relationship'],
                        'child_no' => $family['child_no'],
                        'family_first_name_en' => $family['family_first_name_en'],
                        'family_last_name_en' => $family['family_last_name_en'],
                        'family_first_name_th' => $family['family_first_name_th'],
                        'family_last_name_th' => $family['family_last_name_th'],
                        'family_birth_date' => $family['family_birth_date']
                    ]);
                }
            }
        } catch (Exception $e) {
            return response($e->getMessage(), 500);
        }

        return response()->json('complete');
    }

    public function view(Request $request)
    {
        try {
            $user = Users::with('families')
                ->join('departments', 'users.department_id', 'departments.department_id')
                ->join('agencies', 'users.agency_id', 'agencies.agency_id')
                ->join('positions', 'users.position_id', 'positions.position_id')
                ->select(
                    'users.*',
                    'department_name_en',
                    'position_name_en',
                    'agency_name_en',
                    DB::raw("concat(first_name_en,' ',last_name_en) as emp_en"),
                    DB::raw("concat(first_name_th,' ',last_name_th) as emp_th"),
                )
                ->get();
        } catch (Exception $e) {
            return response($e->getMessage(), 500);
        }

        return response()->json($user);
    }

    public function delete($id)
    {
        try {
            $status = "success";
            $message = "delete user complete";
            Users::where('id', $id)->delete();
            UserFamilies::where('user_id', $id)->delete();
            $data = Agencies::orderBy('agency_name_en')->get();
        } catch (Exception $e) {
            $status = "error";
            $message = $e->getMessage();
            $data = NULL;
        }
        $res = array(
            'status' => $status,
            'message' => $message,
            'data' => $data
        );
        return response()->json($res);
    }

    public function delete_family($family_id)
    {
        try {
            $status = "success";
            $message = "delete family complete";
            UserFamilies::where('family_id', $family_id)->delete();
            $data = NULL;
        } catch (Exception $e) {
            $status = "error";
            $message = $e->getMessage();
            $data = NULL;
        }
        $res = array(
            'status' => $status,
            'message' => $message,
            'data' => $data
        );
        return response()->json($res);
    }

    public function resign(Request $request, $user_id)
    {
        Users::where('id', $user_id)->update([
            'status' => 0,
            'resign_date' => $request->resign_date,
        ]);

        return response()->json('ok');
    }

    public function changepw(Request $request, $user_id)
    {
        Users::where('id', $user_id)->update([
            'password' =>  bcrypt($request->new_pw),
        ]);

        return response()->json('ok');
    }
}
