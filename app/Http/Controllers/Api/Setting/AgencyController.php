<?php

namespace App\Http\Controllers\Api\Setting;

use Illuminate\Http\Request;
use App\Models\Setting\Agencies;
use App\Http\Controllers\Controller;

class AgencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $status = "success";
            $message = "query successfull";
            $data = Agencies::orderBy('agency_name_en')->get();
        } catch (Exception $e) {
            $status = "error";
            $message = $e->getMessage();
            $data = NULL;
        }
        $res = array(
            'status' => $status,
            'message' => $message,
            'data' => $data,
        );
        return response()->json($res);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $status = "success";
            $message = "save new agency complete";
            $data = new Agencies();
            $data->agency_name_en = $request->agency_name_en;
            $data->agency_name_th = $request->agency_name_th;
            $data->agency_status = $request->agency_status;
            $data->save();

            $data = Agencies::orderBy('agency_name_en')->get();
        } catch (Exception $e) {
            $status = "error";
            $message = $e->getMessage();
            $data = NULL;
        }
        $res = array(
            'status' => $status,
            'message' => $message,
            'data' => $data
        );
        return response()->json($res);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $status = "success";
            $message = "query successfull";
            $data = Agencies::where('agency_id', $id)->first();
        } catch (Exception $e) {
            $status = "error";
            $message = $e->getMessage();
            $data = NULL;
        }
        $res = array(
            'status' => $status,
            'message' => $message,
            'data' => $data,
        );
        return response()->json($res);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $status = "success";
            $message = "update agency complete";
            Agencies::where('agency_id', $id)->update([
                'agency_name_en' => $request->agency_name_en,
                'agency_name_th' => $request->agency_name_th,
                'agency_status' => $request->agency_status,
            ]);

            $data = Agencies::orderBy('agency_name_en')->get();
        } catch (Exception $e) {
            $status = "error";
            $message = $e->getMessage();
            $data = NULL;
        }
        $res = array(
            'status' => $status,
            'message' => $message,
            'data' => $data
        );
        return response()->json($res);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $status = "success";
            $message = "delete agency complete";
            Agencies::where('agency_id', $id)->delete();
            $data = Agencies::orderBy('agency_name_en')->get();
        } catch (Exception $e) {
            $status = "error";
            $message = $e->getMessage();
            $data = NULL;
        }
        $res = array(
            'status' => $status,
            'message' => $message,
            'data' => $data
        );
        return response()->json($res);
    }
}
