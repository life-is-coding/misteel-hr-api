<?php

namespace App\Http\Controllers\Api\Setting;

use Illuminate\Http\Request;
use App\Models\Setting\Departments;
use App\Http\Controllers\Controller;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $status = "success";
            $message = "query successfull";
            $data = Departments::orderBy('department_name_en')->get();
        } catch (Exception $e) {
            $status = "error";
            $message = $e->getMessage();
            $data = NULL;
        }
        $res = array(
            'status' => $status,
            'message' => $message,
            'data' => $data,
        );
        return response()->json($res);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $status = "success";
            $message = "save new department complete";
            $data = new Departments();
            $data->department_name_en = $request->department_name_en;
            $data->department_name_th = $request->department_name_th;
            $data->department_status = $request->department_status;
            $data->save();

            $data = Departments::orderBy('department_name_en')->get();
        } catch (Exception $e) {
            $status = "error";
            $message = $e->getMessage();
            $data = NULL;
        }
        $res = array(
            'status' => $status,
            'message' => $message,
            'data' => $data
        );
        return response()->json($res);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $status = "success";
            $message = "query successfull";
            $data = Departments::where('department_id', $id)->first();
        } catch (Exception $e) {
            $status = "error";
            $message = $e->getMessage();
            $data = NULL;
        }
        $res = array(
            'status' => $status,
            'message' => $message,
            'data' => $data,
        );
        return response()->json($res);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $status = "success";
            $message = "update department complete";
            Departments::where('department_id', $id)->update([
                'department_name_en' => $request->department_name_en,
                'department_name_th' => $request->department_name_th,
                'department_status' => $request->department_status,
            ]);

            $data = Departments::orderBy('department_name_en')->get();
        } catch (Exception $e) {
            $status = "error";
            $message = $e->getMessage();
            $data = NULL;
        }
        $res = array(
            'status' => $status,
            'message' => $message,
            'data' => $data
        );
        return response()->json($res);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $status = "success";
            $message = "delete department complete";
            Departments::where('department_id', $id)->delete();
            $data = Departments::orderBy('department_name_en')->get();
        } catch (Exception $e) {
            $status = "error";
            $message = $e->getMessage();
            $data = NULL;
        }
        $res = array(
            'status' => $status,
            'message' => $message,
            'data' => $data
        );
        return response()->json($res);
    }
}
