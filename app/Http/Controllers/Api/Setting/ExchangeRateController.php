<?php

namespace App\Http\Controllers\Api\Setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Setting\ExchangeRate;

class ExchangeRateController extends Controller
{
    public function index()
    {
        try {
            $status = "success";
            $message = "query successfull";
            $data = ExchangeRate::get();
        } catch (Exception $e) {
            $status = "error";
            $message = $e->getMessage();
            $data = NULL;
        }
        $res = array(
            'status' => $status,
            'message' => $message,
            'data' => $data,
        );
        return response()->json($res);
    }

    public function update(Request $request)
    {
        $token = request()->bearerToken();
        $decoded = auth()->user($token);
        $username = $decoded->name;
        try {
            $dt_now = date('Y-m-d H:i:s');
            $status = "success";
            $message = "update position complete";
            ExchangeRate::where('rate_id', '1')->update([
                'exchange_rate' => $request->exchange_rate,
                'updated_at' => $dt_now,
                'updated_by' => $username
            ]);

            $data = ExchangeRate::get();
        } catch (Exception $e) {
            $status = "error";
            $message = $e->getMessage();
            $data = NULL;
        }
        $res = array(
            'status' => $status,
            'message' => $message,
            'data' => $data
        );
        return response()->json($res);
    }
}
