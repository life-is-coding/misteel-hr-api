<?php

namespace App\Http\Controllers\Api\Setting;

use Illuminate\Http\Request;
use App\Models\Setting\Positions;
use App\Http\Controllers\Controller;

class PositionController extends Controller
{
    public function index()
    {
        try {
            $status = "success";
            $message = "query successfull";
            $data = Positions::orderBy('position_name_en')->get();
        } catch (Exception $e) {
            $status = "error";
            $message = $e->getMessage();
            $data = NULL;
        }
        $res = array(
            'status' => $status,
            'message' => $message,
            'data' => $data,
        );
        return response()->json($res);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $status = "success";
            $message = "save new position complete";
            $data = new Positions();
            $data->position_name_en = $request->position_name_en;
            $data->position_name_th = $request->position_name_th;
            $data->position_status = $request->position_status;
            $data->position_level = $request->position_level;
            $data->save();

            $data = Positions::orderBy('position_name_en')->get();
        } catch (Exception $e) {
            $status = "error";
            $message = $e->getMessage();
            $data = NULL;
        }
        $res = array(
            'status' => $status,
            'message' => $message,
            'data' => $data
        );
        return response()->json($res);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $status = "success";
            $message = "query successfull";
            $data = Positions::where('position_id', $id)->first();
        } catch (Exception $e) {
            $status = "error";
            $message = $e->getMessage();
            $data = NULL;
        }
        $res = array(
            'status' => $status,
            'message' => $message,
            'data' => $data,
        );
        return response()->json($res);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $status = "success";
            $message = "update position complete";
            Positions::where('position_id', $id)->update([
                'position_name_en' => $request->position_name_en,
                'position_name_th' => $request->position_name_th,
                'position_status' => $request->position_status,
                'position_level' => $request->position_level,
            ]);

            $data = Positions::orderBy('position_name_en')->get();
        } catch (Exception $e) {
            $status = "error";
            $message = $e->getMessage();
            $data = NULL;
        }
        $res = array(
            'status' => $status,
            'message' => $message,
            'data' => $data
        );
        return response()->json($res);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $status = "success";
            $message = "delete position complete";
            Positions::where('position_id', $id)->delete();
            $data = Positions::orderBy('position_name_en')->get();
        } catch (Exception $e) {
            $status = "error";
            $message = $e->getMessage();
            $data = NULL;
        }
        $res = array(
            'status' => $status,
            'message' => $message,
            'data' => $data
        );
        return response()->json($res);
    }
}
