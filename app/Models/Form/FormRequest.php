<?php

namespace App\Models\Form;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class FormRequest extends Model
{
    public  $table = 'form_request';
    public  $key = 'request_id';
    public  $timestamps = false;
}
