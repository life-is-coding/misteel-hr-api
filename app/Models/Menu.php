<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    public  $table = 'menu';
    public  $key = 'menu_id';
    public  $timestamps = false;

    public function subs()
    {
        return $this->hasMany('App\Models\Menu', 'menu_index', 'menu_index')
            ->select(
                'menu_index',
                'submenu_index',
                'submenu_name',
                DB::raw(" 'sub' as type ")
            )
            ->whereNotNull('submenu_index')
            ->orderBy('sequence');
    }
}
