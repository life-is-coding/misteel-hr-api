<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Model;

class Agencies extends Model
{
    public  $table = 'agencies';
    public  $key = 'agency_id';
    public  $timestamps = false;
}
