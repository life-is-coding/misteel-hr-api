<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Model;

class Departments extends Model
{
    public  $table = 'departments';
    public  $key = 'department_id';
    public  $timestamps = false;
}
