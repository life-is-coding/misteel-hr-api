<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Model;

class ExchangeRate extends Model
{
    public  $table = 'exchangerate';
    public  $key = 'rate_id';
    public  $timestamps = false;
}
