<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Model;

class Positions extends Model
{
    public  $table = 'positions';
    public  $key = 'position_id';
    public  $timestamps = false;
}
