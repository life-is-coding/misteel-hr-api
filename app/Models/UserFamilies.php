<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserFamilies extends Model
{
    public  $table = 'user_families';
    public  $key = 'family_id';
    public  $timestamps = false;
}
