<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    public  $table = 'users';
    public  $key = 'id';
    public  $timestamps = true;

    public function families()
    {
        return $this->hasMany('App\Models\UserFamilies', 'user_id', 'id');
    }
}
