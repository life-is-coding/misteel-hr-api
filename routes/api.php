<?php

use Illuminate\Http\Request;

//header parameters to allow external server to access
// header('Access-Control-Allow-Origin: *');
// header('Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE');
// header('Access-Control-Allow-Headers: Content-Type, X-Auth-Token, Origin, Authorization, Accept, X-Requested-With');

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('cors')->group(function () {
Route::get('/getmenu', 'Api\AuthController@get_menu');
Route::post('/change-lang', 'Api\AuthController@change_lang');
Route::prefix('auth')->group(function () {
    Route::post('/login', 'Api\AuthController@login');
    Route::post('/change-password', 'Api\AuthController@change_password');
});

Route::prefix('setting')->group(function () {
    Route::apiResource('/departments', 'Api\Setting\DepartmentController');
    Route::apiResource('/agencies', 'Api\Setting\AgencyController');
    Route::apiResource('/positions', 'Api\Setting\PositionController');
    Route::get('/exchange-rate', 'Api\Setting\ExchangeRateController@index');
    Route::put('/exchange-rate/{rate_id}', 'Api\Setting\ExchangeRateController@update');
});

Route::prefix('employee')->group(function () {
    Route::get('/active-data', 'Api\Main\EmployeeController@active');
    Route::post('/view', 'Api\Main\EmployeeController@view');
    Route::post('/user/create', 'Api\Main\EmployeeController@create');
    Route::put('/user/{id}', 'Api\Main\EmployeeController@update');
    Route::delete('/user/{id}', 'Api\Main\EmployeeController@delete');
    Route::delete('/family/{family_id}', 'Api\Main\EmployeeController@delete_family');
    Route::post('/user/resign/{user_id}', 'Api\Main\EmployeeController@resign');
    Route::post('/user/changepw/{user_id}', 'Api\Main\EmployeeController@changepw');
});

Route::prefix('form')->group(function () {
    Route::prefix('request')->group(function () {
        Route::get('/', 'Api\Form\RequestController@view');
        Route::post('/', 'Api\Form\RequestController@save');
    });
});
// });


// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Route::controller(OrderController::class)->group(function () {
//     Route::get('/orders/{id}', 'show');
//     Route::post('/orders', 'store');
// });